// JSON2Schema
// File for ID 'schema_id': jsoneditor_app/schema/schema4json.js
// created with JSON2Schema: https://niehausbert.gitlab.io/JSON2Schema

vDataJSON.schema4json =  {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "additionalProperties": true,
    "title": "",
    "definitions": {
        "comment": {
            "title": "Comment:",
            "type": "string",
            "format": "textarea",
            "default": ""
        },
        "yesno": {
            "default": "yes",
            "type": "string",
            "enum": [
                "yes",
                "no"
            ]
        },
        "radio_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            "defaultProperties": [
                "type"
            ],
            "properties": {
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "default": "",
                    "enum":["radiogroup"],
                    "format": "text",
                    "propertyOrder": 20
                }
            }
        },
        "check_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            "defaultProperties": [
                "type"
            ],
            "properties": {
               
                "type": {
                    "type": "string",
                    "enum":["checkbox"],
                    "title": "Option type",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                }                
            }
        },
        "text_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            
            "defaultProperties": [
                "type",
            ],
            "properties": {
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "enum":["text"],
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                }
                
            }
        }
    },
    "type": "object",
    "id": "https://niebert.github.io/json-editor",
    "options": {
        "disable_collapse": false,
        "disable_edit_json": false,
        "disable_properties": false,
        "collapsed": false,
        "hidden": false
    },
    "defaultProperties": [
        "Question_Bank"
    ],
    "properties": {
        "Question_Bank": {
            "type": "array",
            "id": "/properties/Question_Bank",
            "title": "Question Bank",
            "format": "tabs",
            "options": {
                "disable_collapse": false,
                "disable_array_add": false,
                "disable_array_delete": false,
                "disable_array_reorder": false,
                "disable_properties": false,
                "collapsed": false,
                "hidden": false
            },
            "items": {
                "type": "object",
                "id": "/properties/Question_Bank/items",
                "title": "Question",
                "options": {
                    "disable_collapse": false,
                    "disable_edit_json": false,
                    "disable_properties": false,
                    "collapsed": false,
                    "hidden": false
                },
                "defaultProperties": [
                    "question",
                    "media_file",
                    "answer_type",
                    "options",
                    "correct_answer"
                ],
                "properties": {
                    "question": {
                        "type": "string",
                        "id": "/properties/Question_Bank/items/properties/question",
                        "title": "Question",
                        "default": "",
                        "format": "text",
                        "description": "Description for 'question' Type: 'string' Path: '/properties/Question_Bank/items/properties/question'",
                        "options": {
                            "hidden": false
                        },
                        "propertyOrder": 10
                    },
                    
                    
                    "media_file": {
                        "type": "string",
                        "id": "/properties/Question_Bank/items/properties/media_file",
                        "title": "Media File",
                        "default": "",
                        "format": "string",
                        "description": "Description for 'media_file' Type: 'string' Path: '/properties/Question_Bank/items/properties/media_file'",
                        "options": {
                            "hidden": false
                        },
                        "propertyOrder": 20
                    },
                    "answer_type": {
                        //"type": "string",
                        "id": "/properties/Question_Bank/items/properties/answer_type",
                        "title": "Answer Type",
                        //"default": "",
                        "format": "tabs",
                        "oneOf": [
                            {
                              "title": "radiogroup",
                              $ref: "#/definitions/radio_Optionslist"
                            },
                            {
                              "title": "checkbox",
                              $ref: "#/definitions/check_Optionslist"
                            },
                            {
                                "title": "text",
                                $ref: "#/definitions/text_Optionslist"
                            }
                        ],
                        //"description": "Description for 'answer_type' Type: 'string' Path: '/properties/Question_Bank/items/properties/answer_type'",
                       
                        "propertyOrder": 30
                    },
                    "options": {
                        "type": "array",
                        "id": "/properties/Question_Bank/items/properties/options",
                        "title": "Options",
                        "format": "tabs",
                        "options": {
                            "disable_collapse": false,
                            "disable_array_add": false,
                            "disable_array_delete": false,
                            "disable_array_reorder": false,
                            "disable_properties": false,
                            "collapsed": false,
                            "hidden": false
                        },
                        "items": {
                            "type": "object",
                            "id": "/properties/Question_Bank/items/properties/options/items",
                            "title": "Option",
                            "options": {
                                "disable_collapse": false,
                                "disable_edit_json": false,
                                "disable_properties": false,
                                "collapsed": false,
                                "hidden": false
                            },
                            "defaultProperties": [
                                "option",
                                "media_file",
                                "correct_answer"
                            ],
                            "properties": {
                                "option": {
                                    "type": "string",
                                    "id": "/properties/Question_Bank/items/properties/options/items/properties/option",
                                    "title": "Option",
                                    "default": "",
                                    "format": "text",
                                    "description": "Description for 'option' Type: 'string' Path: '/properties/Question_Bank/items/properties/options/items/properties/option'",
                                    "options": {
                                        "hidden": false
                                    },
                                    "propertyOrder": 10
                                },
                                "media_file": {
                                    "type": "string",
                                    "id": "/properties/Question_Bank/items/properties/options/items/properties/media_file",
                                    "title": "Media File",
                                    "default": "",
                                    "format": "",
                                    "description": "Description for 'media_file' Type: 'string' Path: '/properties/Question_Bank/items/properties/options/items/properties/media_file'",
                                    "options": {
                                        "hidden": false
                                    },
                                    "propertyOrder": 20
                                },
                                "correct_answer": {
                                    "type": "boolean",
                                    "id": "/properties/Question_Bank/items/properties/options/items/properties/correct_answer",
                                    "title": "correct_answer",
                                    "default": "",
                                    "format": "boolean",
                                    "description": "Description for 'correct_answer' Type: 'string' Path: '/properties/Question_Bank/items/properties/options/items/properties/correct_answer'",
                                    "options": {
                                        "hidden": false
                                    },
                                    "propertyOrder": 30
                                }
                            }
                        },
                        "propertyOrder": 40
                    }
                }
            },
            "propertyOrder": 10
        }
    }
};
