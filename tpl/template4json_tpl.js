// JSON2Schema
// File for ID 'template_id': jsoneditor_app/tpl/template4json_tpl.js
// created with JSON2Schema: https://niehausbert.gitlab.io/JSON2Schema

vDataJSON.tpl.template4json =  `

  <!-- Object: root --> 
*  
  <!-- Array: root.Question_Bank --> 
<!-- Array Path: root.Question_Bank  -->
{{#each Question_Bank}}
* Sub-Type of Array Element: 'object'
  <!-- Object: root.Question_Bank.* --> 
{{#with this}}
*  {{{question}}}
 <!-- String Format: text --> 
*  {{{media_file}}}
 <!-- String Format: text --> 
*  {{{answer_type}}}
 <!-- String Format: text --> 
*  
  <!-- Array: root.Question_Bank.*.options --> 
<!-- Array Path: root.Question_Bank.*.options  -->
{{#each options}}
* Sub-Type of Array Element: 'object'
  <!-- Object: root.Question_Bank.*.options.* --> 
{{#with this}}
*  {{{option}}}
 <!-- String Format: text --> 
*  {{{media_file}}}
 <!-- String Format: text --> 
{{/with}}
  <!-- Object: root.Question_Bank.*.options.* --> 
{{/each}}
  <!-- Array: root.Question_Bank.*.options --> 
*  {{{correct_answer}}}
 <!-- String Format: text --> 
{{/with}}
  <!-- Object: root.Question_Bank.* --> 
{{/each}}
  <!-- Array: root.Question_Bank --> 
  <!-- Object: root -->`;
