# py -3 -m pip install Flask  (install libraries in python 3.9 syntax)

from distutils.log import error
import re
from flask import Flask, render_template, jsonify, request,redirect
#from flask_cors import CORS 
from flask import *
import json
from flask_cors import CORS, cross_origin
import mysql.connector
import random
from woocommerce import API
import datetime 
from datetime import date, timedelta
import os


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
server_reponse={
	'ack':"",
	'data':""
}

@app.route("/")
@cross_origin()
def home():
    return render_template("index.html")

def write_into_json_file(data_to_write):
	# Serializing json
	json_object = json.dumps(data_to_write, indent=4)

	# Writing to sample.json
	with open("results/"+data_to_write['title']+"_"+data_to_write['user']+".json", "w") as outfile:
		outfile.write(json_object)

@app.route('/collect_data',methods=['POST'])
def collection():
    response={"ack": ""}
    #try:
    data = request.data  
    print(">>",data)        
    j_req_data = json.loads(data)
    write_into_json_file(j_req_data)
    server_reponse['ack']="successfull"
    return jsonify(server_reponse)
    
@app.route('/listfiles', methods=['POST'])
def listfiles():
	basepath = './results'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return jsonify(server_reponse)

@app.route('/getfile', methods=['POST'])
def getfile():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	print(y['file'])
	import os.path
	file_path='./results/'+y['file']
	file_exists = os.path.exists(file_path)
	if(file_exists==True):
		f = open(file_path)
		file_data = json.load(f)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
	else:
		server_reponse['ack']="file not found"
	
	return jsonify(server_reponse)

@app.route('/get_questionnaire', methods=['POST'])
def getQuestionnair():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	print(y['file'])
	import os.path
	file_path='./Questionnaire/'+y['file']
	file_exists = os.path.exists(file_path)
	if(file_exists==True):
		f = open(file_path)
		file_data = json.load(f)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
	else:
		server_reponse['ack']="file not found"
	
	return jsonify(server_reponse)

@app.route('/questionnaire', methods=['POST'])
def questionnaire():
	basepath = './Questionnaire'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return jsonify(server_reponse)


@app.route('/about_data',methods=['POST'])
def about_data():
	
    response={"ack": """
 Development and implementation of sustainable strategies to improve food-safety and retain nutritional values by reducing fungal infestation 
and aflatoxin contamination in the food-chain in Kenya as model region for Sub-Saharan Africa <br> <br>
Multidisciplinary cooperation project between German and African research institutions <br><br>
A fungal toxin that is one of the most toxic natural substances is the subject of the international cooperation project AflaZ: Aflatoxin. 
The research work in the project, coordinated by the Institute for Safety and Quality in Fruit and Vegetables (MRI), ranges from infestation 
of corn plants in the field by aflatoxin-producing fungi, the occurrence of aflatoxins in soils, to insects that spread the fungal spores, 
to cow's milk, in which toxic derivatives of aflatoxin are found.  <br>
AflaZ, which stands for "Zero Aflatoxin", is a research project in which strategies are being developed to reduce the aflatoxin contamination 
of foods frequently consumed in Kenya. Together with scientists from the Friedrich-Löffler-Institut (FLI), the Julius Kühn-Institut (JKI), 
the University of Koblenz-Landau (UKL), and two project partners in Kenya, KALRO (Kenya Agricultural and Livestock Research Organization) 
and EAFF (Eastern African Farmers Federation), the formation of aflatoxin on maize and the contamination of milk resulting from the ingestion 
of contaminated feed maize are being studied. Both foods are popular and widely consumed in sub-Saharan Africa. The project is coordinated 
by PD Dr. Markus Schmidt-Heydt, Institute for Safety and Quality in Fruits and Vegetables at MRI's Karlsruhe site. In addition, two other 
MRI institutes at the Detmold and Kiel sites are involved. 

"""}
    #try:
    data = request.data  
    print(">>",data)        
    
    return jsonify(response)

if __name__ == '__main__':
   app.run()